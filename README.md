# Throwaway Currency Converter

This project is a challenge response to this post [Do you think you know React?](https://dev.to/fleepgeek/react-throwaway-app-1-currency-converter--52c7)

The idea is to recreate easy throwaway projects to practice core aspects of react. This avoids [Tutorial Purgatory](https://dev.to/tonymastrorio/escaping-tutorial-purgatory-as-a-new-developer-1kf3) and maintains the feeling of understanding and improving knowledge of this tool.

## Rules of the project
- Your app should be completed within 60 minutes.
- Must be pure React (no react-router or redux).
- Must delete the project after one week. Why? These are basic apps you should be able to build anytime and not worthy of showcasing as a portfolio for a serious job interview.
- Don't spend much time on designing. Remember, the idea is to check if you think in React. You could style to your taste after 60 minutes.
- Don't look at my solution until you have completed yours. Else, you would be struck with 5 years of 'tutorial purgatory'

> Even though it says to delete after a week I'm keeping on bitbucket to have a knowledge log and try to refactor later :) 

## To run this
```
npm install
```

```
npm start
```

## Known issues
- No treatment of choosing the same currency twice.
- Simple styling