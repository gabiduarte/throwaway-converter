import React, { Component } from 'react';
import './App.css';
import CurrencyConverter from './components/currencyConverter';

class App extends Component {
  render() {
    return (
      <main>
        <h1 className="header">Currency Converter</h1>

        <CurrencyConverter/>
      </main>
    );
  }
}

export default App;
