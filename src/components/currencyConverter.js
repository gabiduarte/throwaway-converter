import React from 'react';
import axios from 'axios';
import './currencyConverter.css';

export default class CurrencyConverter extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
                        amount: '', 
                        currencies: [], 
                        selectedTo: '', 
                        selectedFrom: '',
                        convertedAmount: ''
                    };

        this.renderDropdownCurrency = this.renderDropdownCurrency.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSelectChange = this.handleSelectChange.bind(this);
        this.handleConvert = this.handleConvert.bind(this);
    }
    
    componentWillMount() {
        this.getCurrenciesSymbol();
    }

    getCurrenciesSymbol() {
        axios.get('http://api.openrates.io/latest?base=USD')
            .then((res) => {
                const currencies = Object.getOwnPropertyNames(res.data.rates);
                currencies.push('USD');
                currencies.sort();

                this.setState({...this.state, currencies});
            });
    }

    handleInputChange(event) {
        this.setState({...this.state, amount: event.target.value});
    }

    handleSelectChange(event) {
        const dropdownType = event.target.className;
        const value = event.target.value;

        if (dropdownType === 'from-currency') {
            this.setState({...this.state, selectedFrom: value});
        } else if (dropdownType === 'to-currency') {
            this.setState({...this.state, selectedTo: value});
        }
    }

    handleConvert() {
        const { amount, selectedTo, selectedFrom } = this.state;

        if (!amount || !selectedTo || !selectedFrom ) return;

        axios.get(`http://api.openrates.io/latest?base=${selectedFrom}`)
            .then((res) => {
                const currencyRate = res.data.rates[selectedTo];
                this.setState({...this.state, convertedAmount: this.convertMoney(amount, currencyRate)});
            });
    }

    convertMoney(amount, rate) {
        return (amount * rate).toFixed(2);
    }

    renderDropdownCurrency(selectClass) {
        return (
            <select className={selectClass} onChange={this.handleSelectChange}>
                <option value=""></option>
                {this.state.currencies.map((currency, index) => (
                    <option value={currency} key={index}>{currency}</option>
                ))}
            </select>
        )
    }

    render() {
        return (
            <section className="converter">
                <div className="input-group">
                    convert &nbsp;
                    <input type="number" className="amount" value={this.state.amount} onChange={this.handleInputChange}/>
                    &nbsp; in &nbsp;

                    <div className="select-group">
                        {this.renderDropdownCurrency('from-currency')}
                        &nbsp; &#x02192; &nbsp; 
                        {this.renderDropdownCurrency('to-currency')}
                    </div>

                    <button type="button" onClick={this.handleConvert}>Convert</button>
                </div>

                <div className="result">{this.state.convertedAmount}</div>
            </section>
        )
    }
}